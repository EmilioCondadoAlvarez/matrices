﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matrices
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion;
            Console.WriteLine("Escriba un numero");
            opcion = Int32.Parse(Console.ReadLine());
            switch (opcion)
            {
                case 1:
                    clase1.empleados();
                    break;
                case 2:
                    clase1.letra();
                    break;
                case 3:
                    clase1.mayor_y_menor();
                    break;
                case 4:
                    clase1.ordenamiento();
                    break;
                case 5:
                    clase1.menores_de_columna();
                    break;
                case 6:
                    clase1.mayores_de_fila();
                    break;
            }
        }
    }
}
