﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matrices
{
    class clase1
    {
        public static void empleados()
        {
            string i = "", j = "", nom1 = "Jesus", nom2 = "Cristian", nom3 = "Emilio", nom4 = "Javier", s1 = "1000", s2 = "1500", s3 = "2050", s4 = "5000", st1 = "3000", st2 = "4500", st3 = "6150", st4 = "15000";
            string[] nombres = { i, nom1, nom2, nom3, nom4 };
            string[] salarios = { i, s1, s2, s3, s4 };
            string[,] mat1 = new string[2, 5];
            string[,] mat2 = new string[5, 5];
            Console.WriteLine("Nombres De Los 4 Trabajadores\n");
            Console.WriteLine("Jesus, Cristian, Emilio, Javier\n");
            Console.WriteLine("Salarios De Los Ultimos 3 Meses\n");
            for (int f = 1; f <= 1; f++)
            {
                for (int g = 1; g <= 4; g++)
                {
                    mat1[f, g] = nombres[g];
                    Console.SetCursorPosition(g * 10, f + 7);
                    Console.Write(mat1[f, g]);
                }
            }
            for (int f = 1; f < 4; f++)
            {
                for (int g = 1; g <= 4; g++)
                {
                    mat2[f, g] = salarios[g];
                    Console.SetCursorPosition(g * 10, f + 9);
                    Console.Write(mat2[f, g]);
                }
            }
            Console.WriteLine("\n\n\nGanancias Totales De Los Empleados Durante Los 3 Meses\n");
            Console.WriteLine("Jesus\t" + st1);
            Console.WriteLine("Cristian\t" + st2);
            Console.WriteLine("Emilio\t" + st3);
            Console.WriteLine("Javier\t" + st4);
            Console.WriteLine("Javier Fue El Empleado Con Mas Ganancias");
            Console.ReadKey();

        }
        public static void letra()
        {
            int F = 0, C = 0, N = 0, MI = 0, FI = 0;
            string linea;
            Console.Write("Escriba Un Numero Para Imprimir La R\n:");
            linea = Console.ReadLine();
            N = int.Parse(linea);
            N = (N % 2 == 0 ? N + 1 : N);
            string[,] MAT = new string[N + 1, N + 1];
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    MAT[F, C] = " ";
                }
            }
            MI = N / 2 + 1;
            for (F = 1; F <= N; F++)
            {
                MAT[F, 1] = "R";
                MAT[MI, F] = "R";
                MAT[1, F] = "R";
            }
            FI = MI;
            for (F = 1; F <= MI; F++)
            {
                MAT[F, N] = "R";
                MAT[FI, FI] = "R";
                FI = FI + 1;
            }
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    Console.SetCursorPosition(C, F + 4);
                    Console.Write(MAT[F, C]+ "\n");
                }
            }
            Console.WriteLine();
            Console.Write("Pulse una Tecla:");
            Console.ReadKey();

        }
        public static void mayor_y_menor()
        {
            int F = 0, C = 0, N = 0, MAY = 0, MEN = 0;
            string linea;
            Console.Write("Escriba Un Numero Para Hacer La Matriz:\n");
            Random rnd = new Random();
            linea = Console.ReadLine();
            N = int.Parse(linea);
            int[,] MAT = new int[N + 1, N + 1];
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    MAT[F, C] = rnd.Next(0, 99);
                    Console.SetCursorPosition(C * 4, F + 4);
                    Console.Write(MAT[F, C]);
                }
            }
            MAY = MAT[1, 1];
            MEN = MAT[1, 1];
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    if ((MAT[F, C] > MAY))
                        MAY = MAT[F, C];
                    if ((MAT[F, C] < MEN))
                        MEN = MAT[F, C];
                }
            }
            Console.WriteLine();
            Console.WriteLine("\n Numero Mayor De La Matriz Es: " + MAY);
            Console.WriteLine("\n Numero Menor De la Matriz Es:"  + MEN);
            Console.Write("\nPulse Una Tecla:");
            Console.ReadKey();

        }
        public static void ordenamiento()
        {
            int F = 0, C = 0, I = 0, K = 0, N = 0, AUX = 0;
            string cadena;
            Console.Write("Escriba Un Numero Para Hacer La Matriz:\n");
            cadena = Console.ReadLine();
            Random rnd = new Random();
            N = int.Parse(cadena);
            int[,] MAT = new int[N + 1, N + 1];
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    MAT[F, C] = rnd.Next(0, 100);
                    Console.SetCursorPosition(C * 4, F + 4);
                    Console.Write(MAT[F, C] + "\n");
                }
            }
            for (F = 1; F <= N; F++)
            { 
                for (C = 1; C <= N; C++)
                {
                    for (I = 1; I <= N; I++)
                    {
                        for (K = 1; K <= N; K++)
                        {
                            if ((MAT[F, C] < MAT[I, K]))
                            {
                                AUX = MAT[F, C];
                                MAT[F, C] = MAT[I, K];
                                MAT[I, K] = AUX;
                            }
                        }
                    }
                }
            }
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    Console.SetCursorPosition(C * 4, F + 10);
                    Console.Write( MAT[F, C]);
                }
            }
            Console.WriteLine();
            Console.Write("Pulse una Tecla:");
            Console.ReadLine();
        }
        public static void menores_de_columna()
        {
            int F = 0, C = 0, N = 0, MEN = 0;
            string cadena;
            Console.Write("Escriba Un Numero Para Hacer La Matriz:\n");
            cadena = Console.ReadLine();
            N = int.Parse(cadena);
            int[,] MAT = new int[100, 100];
            int[] VEC = new int[N + 1];
            Random rnd = new Random();
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    MAT[F, C] = rnd.Next(0, 100);
                    Console.SetCursorPosition(C * 4, F + 4);
                    Console.Write(MAT[F, C]);
                }
            }
            for (C = 1; C <= N; C++)
            {
                MEN = MAT[1, C];
                for (F = 1; F <= N; F++)
                {
                    if ((MAT[F, C] < MEN))
                        MEN = MAT[F, C];
                }
                VEC[C] = MEN;
            }
            for (C = 1; C <= N; C++)
            {
                Console.SetCursorPosition(C * 4, 15);
                Console.Write(VEC[C]);
            }
            Console.WriteLine();
            Console.Write("Pulse una Tecla:");
            Console.ReadLine();
        }
        public static void mayores_de_fila()
        {
            int F = 0, C = 0, N = 0, MAY = 0;
            string cadena;
            Console.Write("Escriba Un Numero Para Hacer La Matriz: ");
            cadena = Console.ReadLine();
            N = int.Parse(cadena);
            int[,] MAT = new int[50, 50];
            int[] VEC = new int[N + 1];
            Random rnd = new Random();
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    MAT[F, C] = rnd.Next(0, 100);
                    Console.SetCursorPosition(C * 4, F +4);
                    Console.Write(MAT[F, C]);
                }
            }
            for (F = 1; F <= N; F++)
            {
                MAY = MAT[F, 1];
                for (C = 1; C <= N; C++)
                {
                    if (MAT[F, C] > MAY)
                        MAY = MAT[F, C];
                }
                VEC[F] = MAY;
            }
            for (F = 1; F <= N; F++)
            {
                Console.SetCursorPosition(50, F );
                Console.WriteLine(VEC[F]);
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.Write("Pulse una Tecla:");
            Console.ReadLine();
        }
    }
}
